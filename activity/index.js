let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
};

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
};

const {name, birthday, age, isEnrolled, classes} = student1;
const [class1, class2] = classes;

const introduce = (student) => {

	console.log(`Hi! I'm ${name}. I am ${age} years old.`);
	console.log(`I study the following courses: ${class1} and ${class2}.`);
}

introduce();




getCube = (num) => console.log(Math.pow(num,3));

getCube(3);




let numArr = [15,16,32,21,21,2];


const [num1, num2, num3, num4, num5, num6] = numArr;

numArr.forEach(num => console.log(num));




let numsSquared = numArr.map(num => Math.pow(num,2))

console.log(numsSquared);



class dog {
	constructor(name, breed, year){
		this.name = name;
		this.breed = breed;
		this.dogAge = year * 7
	}
}

let dog1 = new dog("Lebron", "Dachshund", 1.5);
console.log(dog1);

let dog2 = new dog("Curry", "Shih Tzu", 3);
console.log(dog2);